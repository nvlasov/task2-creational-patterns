package abstractfactory.factory;

import abstractfactory.entity.ingredient.ChicagoCheese;
import abstractfactory.entity.ingredient.*;

public class ChicagoPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new WideDough();
    }

    @Override
    public Sauce createSauce() {
        return new TomatoSauce();
    }

    @Override
    public Cheese createCheese() {
        return new ChicagoCheese();
    }
}
