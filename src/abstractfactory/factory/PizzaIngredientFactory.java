package abstractfactory.factory;

import abstractfactory.entity.ingredient.Cheese;
import abstractfactory.entity.ingredient.Dough;
import abstractfactory.entity.ingredient.Sauce;

public interface PizzaIngredientFactory {

    public Dough createDough();
    public Sauce createSauce();
    public Cheese createCheese();
}