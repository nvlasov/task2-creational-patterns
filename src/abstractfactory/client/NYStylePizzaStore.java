package abstractfactory.client;

import abstractfactory.entity.*;
import abstractfactory.factory.NYPizzaIngredientFactory;
import abstractfactory.factory.PizzaIngredientFactory;

public class NYStylePizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(String item) {
        Pizza pizza = null;
        PizzaIngredientFactory factory = new NYPizzaIngredientFactory();
        if (item.equals("cheese")) {
            pizza = new CheesePizza(factory);
            pizza.setName("NY Cheese Pizza");
        } else if(item.equals("veggie")) {
            pizza = new VeggiePizza(factory);
            pizza.setName("NY Veggie Pizza");
        }
        return pizza;
    }
}
