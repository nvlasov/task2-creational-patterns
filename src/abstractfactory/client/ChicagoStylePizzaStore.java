package abstractfactory.client;

import abstractfactory.entity.CheesePizza;
import abstractfactory.entity.Pizza;
import abstractfactory.entity.VeggiePizza;
import abstractfactory.factory.ChicagoPizzaIngredientFactory;
import abstractfactory.factory.PizzaIngredientFactory;

public class ChicagoStylePizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(String item) {
        Pizza pizza = null;
        PizzaIngredientFactory factory = new ChicagoPizzaIngredientFactory();
        if (item.equals("cheese")) {
            pizza = new CheesePizza(factory);
            pizza.setName("Chi Cheese Pizza");
        } else if(item.equals("veggie")) {
            pizza = new VeggiePizza(factory);
            pizza.setName("Chi Veggie Pizza");
        }
        return pizza;
    }
}
