package abstractfactory;

import abstractfactory.client.ChicagoStylePizzaStore;
import abstractfactory.client.NYStylePizzaStore;
import abstractfactory.client.PizzaStore;
import abstractfactory.entity.Pizza;

public class TestPizza {

    public static void main(String[] args) {
        PizzaStore nyStore = new NYStylePizzaStore();
        PizzaStore chicagoStore = new ChicagoStylePizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("1st order is " + pizza.getName());

        pizza = chicagoStore.orderPizza("veggie");
        System.out.println("2nd order is " + pizza.getName());

    }

}
