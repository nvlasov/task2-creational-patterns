package abstractfactory.entity;

import abstractfactory.entity.ingredient.Cheese;
import abstractfactory.entity.ingredient.Dough;
import abstractfactory.entity.ingredient.Sauce;

public abstract class Pizza {
    String name;
    Dough dough;
    Sauce sauce;
    Cheese cheese;

    public abstract void prepare();

    public void bake() {
        System.out.println("Bake for 25 min at 350");
    }

    public void cut() {
        System.out.println("Cutting the pizza diagonal slices");
    }

    public void box() {
        System.out.println("Official PizzaStore box");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
