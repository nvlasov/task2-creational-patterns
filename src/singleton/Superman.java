package singleton;

/**
 * Static field. Thread safe. KISS. Not lazy init.
 */
public class Superman {
    public static final Superman INSTANCE = new Superman();
    private Superman() {

    }
}
