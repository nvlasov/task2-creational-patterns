package singleton;

public class TestSingleton {

    /**
     * Use normal, not lazy initialization everywhere where it is possible.
     * For static fields use On Demand Holder idiom.
     * For usual fields use Double Checked Locking & volatile.
     * Everywhere else - Synchronized Accessor.
     */


}
