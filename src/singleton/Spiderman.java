package singleton;

/**
 * Enum singleton. Serialization, thread safe from the Java box. Not lazy
 */
public enum Spiderman {
    INSTANCE("Spiderman");

    private String name;

    Spiderman(String name) {
        this.name = name;
    }

    public static Spiderman getInstance() {
        return INSTANCE;
    }
}
