package singleton;

/**
 * On Demand Holder idiom. Lazy. High performance. Only for static fields.
 */
public class Hulk {
    public static class SingletonHolder {
        public static final Hulk HOLDER_INSTANCE = new Hulk();
    }

    public static Hulk getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }
}
