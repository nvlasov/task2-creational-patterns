package singleton;

/**
 *  Double Checked Locking & volatile. Lazy. High performance. From JDK 1.5
 */
public class Punisher {
    private static volatile Punisher instance;

    public static Punisher getInstance() {
        Punisher localInstance = instance;
        if (localInstance == null) {
            synchronized (Punisher.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Punisher();
                }
            }
        }
        return localInstance;
    }
}
