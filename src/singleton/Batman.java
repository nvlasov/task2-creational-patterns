package singleton;

/**
 * Synchronized Accessor. Lazy. Low performance.
 */
public class Batman {

    private static Batman instance;

    public static synchronized Batman getInstance() {
        if (instance == null) {
            instance = new Batman();
        }
        return instance;
    }

}
